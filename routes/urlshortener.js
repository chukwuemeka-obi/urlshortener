const express = require('express')
const router = express.Router()
const UrlshortenerController = require('../controllers/Urlshortener')

router.get('/', UrlshortenerController.home)

router.post('/convertToShortUrl', UrlshortenerController.generateShortUrl)

router.get('/:shortUrl', UrlshortenerController.visitShortUrl)

module.exports = router

