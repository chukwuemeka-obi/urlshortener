const express = require('express')
require('./database')
const UrlshortenerRoutes = require('./routes/urlshortener')

const app = express()

app.use(express.urlencoded({ extended: true }))
app.set('view engine', 'ejs')
app.use('/', UrlshortenerRoutes)


const port = process.env.PORT || 5000
app.listen(port, () => console.log(`Urlshortener app running at http://localhost:${port}`))
