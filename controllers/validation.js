const Joi = require('joi');

const validateUrl = (url) => {
    const schema = Joi.object({
        longUrl: Joi.string().uri().label("Long Url").required(),
    })

    return schema.validate(url);
}

module.exports = validateUrl