const Urlshortener = require('../models/urlshortener')
const validateUrl = require('./validation')

let UrlshortenerController = {}

UrlshortenerController.home =  async (req, res) => {
    const shortUrls = await Urlshortener.findAll()
    res.render('index', { shortUrls: shortUrls })
}

UrlshortenerController.generateShortUrl = async (req, res) => {
    try {
        
        const {error} = validateUrl(req.body)
        if (error) {
            return res.redirect('/')
        }

        await Urlshortener.create({
            longUrl: req.body.longUrl
        })
        
        res.redirect('/')
        
    } catch (err) {
        if (err.name === 'SequelizeValidationError') {
            const errObj = {};
            err.errors.map( er => {
            errObj[er.path] = er.message;
            })
            res.redirect('/');
        }
    }
}

UrlshortenerController.visitShortUrl = async (req, res) => {
    const shortUrl = await Urlshortener.findOne({
        where: {
            shortUrl: req.params.shortUrl
        }
    })

    if (shortUrl === null) return res.sendStatus(404)

    shortUrl.visitCount++
    shortUrl.save()

    res.redirect(shortUrl.longUrl)
}

module.exports = UrlshortenerController
