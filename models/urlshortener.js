const db = require('../database')
const shortId = require('shortid')

const UrlShortener = db.define('UrlShortener', {
    // Model attributes are defined here
    longUrl: {
      type: db.DataTypes.STRING,
      allowNull: false,
      validate: {
        isUrl: true, 
      }
    },
    shortUrl: {
      type: db.DataTypes.STRING,
      allowNull: false,
      defaultValue: shortId.generate
    },
    visitCount: {
        type: db.DataTypes.INTEGER,
        defaultValue: 0
    }
  }, {
    // Other model options go here
    timestamps: true
});

(async () => {
    await db.sync({ force: true });
    console.log("All models were synchronized successfully.");
})();

module.exports = UrlShortener
